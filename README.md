# Izposodi.si

**Avtorji:** Sara, Lara, Saša, Jure, Matija

**Tehnologije:** Spring boot, Java, html, css (bootstrap), js, SQL 

**Razvojno okolje:** Intellij, MySQL workbench, XAMPP 

**Kanban Tabla:** https://trello.com/b/7B12jEEt/praktikum

**Funkcionalnosti:** Login/registracija, Admin, dodajanje izdelkov, izposoja izdelkov, evidenca izposojenih izdelkov, vračanje izdelkov, 
kreiranje QR kode za izposojo, skeniranje QR kode, prikaz podatkov o izposoji

**Mapa projekta:** Praktikum

**Namen projekta:** Vodenje evidence izdelkov namenjenih za izposojo in izposojenih izdelkov.

**Instalacija:**
* Razvojno okolje
  * Uvoz datoteke Praktikum v razvojno okolje IntelliJ IDEA 2019.1.3 (ali kasnejše)
* Baza
  * V MySQL Workbench 8.0 ustvarimo novi Connection. Pomoč pri ustvarjanju: https://dev.mysql.com/doc/workbench/en/wb-getting-started-tutorial-create-connection.html
  * Connection Name: jdbc:mysql://localhost:3306/praktikum
  * Username: root
  * (Data-Source Properties so zapisane v praktikum2020\Praktikum\src\main\resources\application.properties)
  * (Ko imate kreairan nov connection, morate v lokalni bazi ustvariti nov database --->"CREATE DATABASE praktikum;")
  * Za povezavo z bazo je potreben tudi Wampserver, ki si ga lahko prenesete tukaj: https://sourceforge.net/projects/wampserver/
  * ![ezgif.com-gif-maker](/uploads/997afae44d2b7cded03b3b295581ae29/ezgif.com-gif-maker.gif)
  

* Zagon
  * Spletno aplikacijo zaženemo v praktikum2020\Praktikum\src\main\java\demo\RunApplication.java z desnim klikom v RunApplication.java in nato s klikom na Run 'RunApplication'
  * V brskalniku odpremo http://localhost:8080/
  * ![ezgif.com-resize](/uploads/1a2e576d63c6157d20bc33551c873e5b/ezgif.com-resize.gif)
  

 **Admin:**
* Uporabniško ime: admin@gmail.com
* Geslo: geslozaadmina

**Lokacija posameznih sklopov datotek:**
* SecurityConfiguration (praktikum2020\Praktikum\src\main\java\demo\config\SecurityConfiguration.java)
* Controllers (praktikum2020\Praktikum\src\main\java\demo\controller)
* ZXingHelper (praktikum2020\Praktikum\src\main\java\demo\helpers)
* Models (praktikum2020\Praktikum\src\main\java\demo\model)
* Services (praktikum2020\Praktikum\src\main\java\demo\service)
* Repositories (praktikum2020\Praktikum\src\main\java\demo\service\repository)
* Preverjanje Zamude (praktikum2020\Praktikum\src\main\java\demo\PreverjanjeZamude.java)
* JSP (praktikum2020\Praktikum\src\main\webapp\WEB-INF\jsp)
* Application Properties (praktikum2020\Praktikum\src\main\resources\application.properties)
* Run Application (praktikum2020\Praktikum\src\main\java\demo\RunApplication.java)