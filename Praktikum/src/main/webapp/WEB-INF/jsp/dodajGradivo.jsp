<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Dodajanje gradiva</title>

    <script src="assets/js/capture.js"></script>

    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/capture.css" rel="stylesheet">


    <!-- =======================================================
    * Template Name: Arsha - v2.0.0
    * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

        <h1 class="logo mr-auto"><a href="index">Domov</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li><a href="index">Home</a></li>
                <% if(session.getAttribute("email") == null) { %>
                <li><a href="registracija">Registracija</a></li>
                <li><a href="prijava">Prijava</a></li>
                <%  } else  { %>
                <li><a href="#">Pozdravljeni <%  out.print(session.getAttribute("email")); %></a></li>
                <li class="active"><a href="gradiva">Gradiva</a></li>
                <li><a href="/odjava">Odjava</a></li>
                <% }%>
            </ul>
        </nav><!-- .nav-menu -->
    </div>
</header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
            </div>
            <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
                <img src="assets/img/hero-img.png" class="img-fluid animated" alt="">
            </div>
        </div>
    </div>

</section><!-- End Hero -->

<main id="main">
    <% if(session.getAttribute("email") != null) { %>
    <div class="container" data-aos="fade-up">
        <form method="get" action="dodajGradiv">
            <h2>Dodaj gradivo</h2>
            <table>
                <tr>
                    <td>
                        <label>Tip gradiva: </label>
                        <select name="sifrant" required>
                            <option value="-1">Izberi vrsto gradiva</option>
                            <%
                                try
                                {
                                    String Query = "Select naziv_tipa_gradiva from tip_gradiva";
                                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                                    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/praktikum", "root", "");
                                    Statement stm = conn.createStatement();
                                    ResultSet rs = stm.executeQuery(Query);
                                    while(rs.next()) {
                            %>
                            <option value="<%=rs.getString("naziv_tipa_gradiva")%>"><%=rs.getString("naziv_tipa_gradiva")%></option>
                            <%
                                    }
                                }
                                catch (Exception ex){
                                    ex.printStackTrace();
                                    out.println("Error"+ex.getMessage());
                                }

                            %>
                        </select>
                    <td>
                        <label>Naziv gradiva: </label>
                        <input type="text" name="naziv" class="form-control" id="naziv" minlength="3" required/>
                    </td>
                    <td>
                        <label>Opis: </label>
                        <input type="text" name="opis" class="form-control" id="opis" minlength="10" required/>
                    </td>
                    <!-- <td>
                        <label>Slika: </label>
                        <div class="contentarea">
                            <div class="camera">
                                <video id="video">Video stream not available.</video>
                                <button id="startbutton">Take photo</button>
                            </div>
                            <canvas id="canvas">
                            </canvas>
                            <div class="output">
                                <img id="photo" alt="The screen capture will appear in this box." value="photo">
                            </div>
                        </div>

                    </td> -->
                    <td>
                        <!--  <input name="photo" value="${photo}" type="hidden" class="form-control" id="gradivo" /> -->
                        <input name="uporabnik" value="${email}"  type="hidden" class="form-control" id="uporabnik" />
                    </td>
                    </td>
                    <td>
                        <button type="submit" class="btn btn-primary">Dodaj gradivo</button>
                    </td>
                </tr>
            </table>
        </form>

    </div>
    <%  } else  { %>

    <% }%>


</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 footer-contact">
                    <h3>Praktikum 2020</h3>
                    <p>
                        <strong>Email:</strong> info@example.com<br>
                    </p>
                </div>

            </div>
        </div>
    </div>

    <div class="container footer-bottom clearfix">
        <div class="copyright">
            &copy; Copyright <strong><span>Arsha</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/ -->
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
<div id="preloader"></div>

<!-- Vendor JS Files -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>
<script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/venobox/venobox.min.js"></script>
<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="assets/vendor/aos/aos.js"></script>

<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>

</body>

</html>