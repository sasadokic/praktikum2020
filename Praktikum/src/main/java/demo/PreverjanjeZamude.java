package demo;

import demo.service.EmailSenderService;
import demo.service.repository.IzposojaRepozitorij;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

public class PreverjanjeZamude implements Job {

    @Autowired
    IzposojaRepozitorij izposojaRepozitorij;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        try
        {
            String Query = "Select * from izposoja";
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/praktikum", "root", "");
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(Query);
            while(rs.next()) {
                String datum = rs.getString("datumvrnitve");

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String zdaj = sdf.format(new Date());

                Calendar c = Calendar.getInstance();
                try {
                    c.setTime(sdf.parse(datum));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                c.add(Calendar.DATE, 1);  // number of days to add
                datum = sdf.format(c.getTime());  // dt is now the new date

                if (datum.equals(zdaj)) {

                    String email = rs.getString("izposojevalec");


                    Properties props = new Properties();
                    props.put("mail.smtp.host", "smtp.gmail.com");
                    props.put("mail.smtp.socketFactory.port", "465");
                    props.put("mail.smtp.socketFactory.class",
                            "javax.net.ssl.SSLSocketFactory");
                    props.put("mail.smtp.auth", "true");
                    props.put("mail.smtp.port", "465");
                    //get Session
                    Session session = Session.getDefaultInstance(props,
                            new javax.mail.Authenticator() {
                                protected PasswordAuthentication getPasswordAuthentication() {
                                    return new PasswordAuthentication("izposodi.si@gmail.com","geslozaadmina");
                                }
                            });
                    //compose message
                    try {
                        MimeMessage message = new MimeMessage(session);
                        message.addRecipient(Message.RecipientType.TO,new InternetAddress(email));
                        message.setSubject("Zamuda");
                        message.setText("Rok za vrnitev gradiva "+ rs.getString("gradivo") + " je potekel. Čimprej vrni gradivo :)");
                        //send message
                        Transport.send(message);
                        System.out.println("email je poslan");
                    } catch (MessagingException e) {throw new RuntimeException(e);}

                }

            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        catch (Exception ex){
            ex.printStackTrace();
            System.out.println("Error "+ex.getMessage());
        }




    }
}
