package demo.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import demo.model.Gradivo;
import demo.service.repository.GradivoRepozitorij;

@Service("productService")
public class ProductServiceImpl implements ProductService{

    @Autowired
    private GradivoRepozitorij gradivoRepozitorij;

    @Override
    public Iterable<Gradivo> findAll() {
        return gradivoRepozitorij.findAll();
    }

}
