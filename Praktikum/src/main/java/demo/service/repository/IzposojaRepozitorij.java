package demo.service.repository;

import demo.model.Admin;
import demo.model.Izposoja;
import org.springframework.data.repository.CrudRepository;

public interface IzposojaRepozitorij extends CrudRepository<Izposoja, Integer> {
}
