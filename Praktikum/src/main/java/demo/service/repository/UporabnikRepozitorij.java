package demo.service.repository;


import demo.model.Uporabnik;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface UporabnikRepozitorij extends CrudRepository<Uporabnik, Long> {
    Uporabnik findByEmailIgnoreCase(String email);
    Uporabnik findByGeslo(String geslo);

}
