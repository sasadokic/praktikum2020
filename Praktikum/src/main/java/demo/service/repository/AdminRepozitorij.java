package demo.service.repository;


import demo.model.Admin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("adminRepozitorij")
public interface AdminRepozitorij extends CrudRepository<Admin, String> {


}