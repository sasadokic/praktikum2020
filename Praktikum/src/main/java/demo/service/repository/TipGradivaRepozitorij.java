package demo.service.repository;

import demo.model.TipGradiva;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipGradivaRepozitorij extends CrudRepository<TipGradiva, String> {
}
