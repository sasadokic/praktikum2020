package demo.service.repository;

import demo.model.Gradivo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Repository("gradivoRepozitorij")
public interface GradivoRepozitorij extends CrudRepository<Gradivo, String> {
}
