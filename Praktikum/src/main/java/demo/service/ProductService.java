package demo.service;

import demo.model.Gradivo;

public interface ProductService {

    public Iterable<Gradivo> findAll();

}
