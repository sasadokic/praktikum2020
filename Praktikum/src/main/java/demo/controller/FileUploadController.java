package demo.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import static demo.helpers.QRCodeReader.convert;
import static demo.helpers.QRCodeReader.decodeQRCode;

@Controller
public class FileUploadController {
    public static String uploadDirectory = System.getProperty("user.dir")+"/uploads";

    @RequestMapping("/uploadView")
    public String uploadView(Model model) {
        return "uploadView";
    }

    @RequestMapping("/upload")

    String upload(Model model, @RequestParam("files") MultipartFile files) {

        String decodedText = null;


        try {

            decodedText = decodeQRCode(convert(files));
            System.out.println(decodedText);
            model.addAttribute("decodedText", decodedText);



        } catch (IOException e) {
            System.out.println("Could not decode QR Code, IOException :: " + e.getMessage());
        }


        return "redirect:" + decodedText;
    }


}

