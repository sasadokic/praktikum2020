package demo.controller;

import demo.model.Admin;
import demo.model.TipGradiva;
import demo.service.repository.AdminRepozitorij;
import demo.service.repository.TipGradivaRepozitorij;
import demo.service.repository.UporabnikRepozitorij;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

@Controller
public class AdminController {

    Logger logger = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private UporabnikRepozitorij uporabnikRepozitorij;

    @Autowired
    private AdminRepozitorij adminRepozitorij;

    @Autowired
    private TipGradivaRepozitorij tipGradivaRepozitorij;

    @RequestMapping(value = {"/prijavaAdmin"}, method = RequestMethod.GET)
    public String prijavaAdmin(Model model) {
        return "prijavaAdmin";
    }

    @RequestMapping(value = {"/preveri"}, method = RequestMethod.GET)
    public String preveriAdmina(Model model,
                                @RequestParam(value = "email") String email,
                                @RequestParam(value = "geslo") String geslo
    ) {
        if(email.equals("admin@gmail.com") && geslo.equals("geslozaadmina"))
        {
            Admin admin = new Admin();
            admin.setEmail(email);
            admin.setGeslo(geslo);

            adminRepozitorij.save(admin);
            model.addAttribute("login", "Uspešna admin prijava");

            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            HttpSession session = request.getSession(true);

            session.setAttribute("email", email);

            return "indexAdmin";
        }
        else {
            return "prijavaAdmin";
        }

    }

    @RequestMapping(value = "/odjavaAdmin", method = RequestMethod.GET)
    public String logout(Model model) {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);

        session.invalidate();

        return "indexAdmin";
    }

    @RequestMapping(value = {"/indexAdmin"}, method = RequestMethod.GET)
    public String indexAdmin(Model model) {
        return "indexAdmin";
    }

    @RequestMapping(value = {"/adminUporabniki"}, method = RequestMethod.GET)
    public String adminUporabniki(Model model) {
        model.addAttribute("uporabnik", uporabnikRepozitorij.findAll());

        return "adminUporabniki";
    }

    @RequestMapping(value = {"/adminSifranti"}, method = RequestMethod.GET)
    public String adminSifranti(Model model) {
        model.addAttribute("tipi", tipGradivaRepozitorij.findAll());

        return "adminSifranti";
    }

    @RequestMapping(value = {"/dodajSifrant"}, method = RequestMethod.GET)
    public String adminDodanSifrant(Model model,
                                    @RequestParam(value = "naziv_tipa_gradiva") String naziv_tipa_gradiva
                                    ) {
        tipGradivaRepozitorij.save(new TipGradiva(naziv_tipa_gradiva));
        model.addAttribute("nov_tip", naziv_tipa_gradiva);

        return "adminDodanSifrant";
    }



    @RequestMapping (value ={"izbrisUporabnika"}, method = RequestMethod.GET)
    public String izbrisi(Model model,
                          @RequestParam(value = "odstrani") Long id) {

        try
        {
            String Query = "delete * from confirmation_token where id_uporabnik="+id;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/praktikum", "root", "");
            Statement stmt = conn.createStatement();
            String sql = "DELETE FROM confirmation_token " + "WHERE id_uporabnik="+id;
            stmt.executeUpdate(sql);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        catch (Exception ex){
            ex.printStackTrace();
            System.out.println("Error "+ex.getMessage());
        }

        uporabnikRepozitorij.delete(id);

        model.addAttribute("uporabniki", uporabnikRepozitorij.findAll());
        return "adminUporabniki";
    }

    public UporabnikRepozitorij getUporabnikRepozitorij() {
        return uporabnikRepozitorij;
    }

    public void setUporabnikRepozitorij(UporabnikRepozitorij uporabnikRepozitorij) {
        this.uporabnikRepozitorij = uporabnikRepozitorij;
    }

    public AdminRepozitorij getAdminRepozitorij() {
        return adminRepozitorij;
    }

    public void setAdminRepozitorij(AdminRepozitorij adminRepozitorij) {
        this.adminRepozitorij = adminRepozitorij;
    }

    public TipGradivaRepozitorij getTipGradivaRepozitorij() {
        return tipGradivaRepozitorij;
    }

    public void setTipGradivaRepozitorij(TipGradivaRepozitorij tipGradivaRepozitorij) {
        this.tipGradivaRepozitorij = tipGradivaRepozitorij;
    }
}
