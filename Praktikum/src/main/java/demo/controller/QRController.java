package demo.controller;

import java.io.OutputStream;
import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;


import demo.model.Gradivo;
import demo.service.repository.GradivoRepozitorij;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import demo.helpers.ZXingHelper;
import demo.service.ProductService;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("gradivo")
public class QRController {



    @Autowired
    GradivoRepozitorij gradivoRepozitorij;





    @RequestMapping(value = "qrcode/{id_izposoja}", method = RequestMethod.GET)
    public void qrcode(@PathVariable("id_izposoja") Integer id, HttpServletResponse response) throws Exception {
        response.setContentType("image/png");
        OutputStream outputStream = response.getOutputStream();
        outputStream.write(ZXingHelper.getQRCodeImage("http://localhost:8080/qrIzposoja/"+id ,200, 200));
        outputStream.flush();
        outputStream.close();
    }









}
