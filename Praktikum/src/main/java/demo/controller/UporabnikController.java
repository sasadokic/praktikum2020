package demo.controller;


import demo.model.Admin;
import demo.model.ConfirmationToken;
import demo.model.Uporabnik;
import demo.service.EmailSenderService;
import demo.service.repository.AdminRepozitorij;
import demo.service.repository.ConfirmationTokenRepository;
import demo.service.repository.UporabnikRepozitorij;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import org.springframework.security.crypto.bcrypt.BCrypt;

@Controller
public class UporabnikController {

    @Autowired
    private UporabnikRepozitorij uporabnikRepozitorij;


    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    private EmailSenderService emailSenderService;

    @RequestMapping(value = "/registracija", method = RequestMethod.GET)
    public ModelAndView displayRegistration(ModelAndView modelAndView, Uporabnik uporabnik) {
        modelAndView.addObject("uporabnik", uporabnik);
        modelAndView.setViewName("registracija");
        return modelAndView;
    }

    @RequestMapping(value = "/registracija", method = RequestMethod.POST)
    public ModelAndView registerUser(ModelAndView modelAndView, Uporabnik uporabnik) {
        Uporabnik obstojecUporabnik = uporabnikRepozitorij.findByEmailIgnoreCase(uporabnik.getEmail());
        if (obstojecUporabnik != null) {
            modelAndView.addObject("message", "Uporabnik s tem emailom že obstaja!");
            modelAndView.setViewName("napaka");
        } else {
            uporabnikRepozitorij.save(uporabnik);
            String salt = BCrypt.gensalt(12);
            uporabnik.setSalt(salt);
            uporabnik.setGeslo(BCrypt.hashpw(uporabnik.getGeslo(), salt));
            ConfirmationToken confirmationToken = new ConfirmationToken(uporabnik);

            confirmationTokenRepository.save(confirmationToken);

            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(uporabnik.getEmail());
            mailMessage.setSubject("Dokončanje registracije!");
            mailMessage.setFrom("chand312902@gmail.com");
            mailMessage.setText("Za aktivacijo svojega računa prosim kliknite na naslednji link: "
                    + "http://localhost:8080/potrdi-registracijo?token=" + confirmationToken.getConfirmationToken());

            emailSenderService.sendEmail(mailMessage);

            modelAndView.addObject("email", uporabnik.getEmail());

            modelAndView.setViewName("uspesnaRegistracija");
        }

        return modelAndView;
    }

    @RequestMapping(value = "/potrdi-registracijo", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView confirmUserAccount(ModelAndView modelAndView, @RequestParam("token") String confirmationToken) {
        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

        if (token != null) {
            Uporabnik uporabnik = uporabnikRepozitorij.findByEmailIgnoreCase(token.getUporabnik().getEmail());
            uporabnik.setEnabled(true);
            uporabnikRepozitorij.save(uporabnik);
            modelAndView.setViewName("registracijaPotrjena");
        } else {
            modelAndView.addObject("message", "Aktivacijski link ne deluje!");
            modelAndView.setViewName("napaka");
        }

        return modelAndView;
    }


    @RequestMapping(value = "/pozabljenoGeslo", method = RequestMethod.POST)
    public ModelAndView forgottenPasswordUser(ModelAndView modelAndView, Uporabnik uporabnik) {
        Uporabnik obstojecUporabnik = uporabnikRepozitorij.findByEmailIgnoreCase(uporabnik.getEmail());
        if (obstojecUporabnik != null) {

            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(uporabnik.getEmail());
            mailMessage.setSubject("Pozabljeno geslo!");
            mailMessage.setFrom("chand312902@gmail.com");
            mailMessage.setText("Za spreminajnje gesla prosim kliknite na naslednji link: "
                    + "http://localhost:8080/spremeniGeslo");
            emailSenderService.sendEmail(mailMessage);

            modelAndView.setViewName("obvestiloGeslo");

        } else {

            modelAndView.addObject("email", uporabnik.getEmail());

            modelAndView.setViewName("napaka");
        }

        return modelAndView;
    }

    @RequestMapping(value = "/spremeniGeslo", method = RequestMethod.POST)
    public ModelAndView changePasswordUser(ModelAndView modelAndView, Uporabnik uporabnik) throws InvalidKeySpecException, NoSuchAlgorithmException {

        Uporabnik obstojecUporabnik = uporabnikRepozitorij.findByEmailIgnoreCase(uporabnik.getEmail());
        if (obstojecUporabnik != null) {

            String novoGeslo = uporabnik.getGeslo();
            obstojecUporabnik.setGeslo(novoGeslo);

            uporabnikRepozitorij.save(obstojecUporabnik);


            modelAndView.addObject("message", "Geslo je bilo uspešno spremenjeno!");
            modelAndView.setViewName("prijava");
        } else {

            modelAndView.addObject("email", uporabnik.getEmail());

            modelAndView.setViewName("Prišlo je do napake!");
        }

        return modelAndView;
    }

    @RequestMapping(value = "/prijava", method = RequestMethod.POST)
    public String login(Model model, Uporabnik uporabnik) {

        Uporabnik obstojecUporabnik = uporabnikRepozitorij.findByEmailIgnoreCase(uporabnik.getEmail());

        // hashing pass
        String hashGeslo = BCrypt.hashpw(uporabnik.getGeslo(), obstojecUporabnik.getSalt());

        if (uporabnik.getEmail().equals(obstojecUporabnik.getEmail()) && hashGeslo.equals(obstojecUporabnik.getGeslo()) && obstojecUporabnik.isEnabled()) {

            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            HttpSession session = request.getSession(true);

            session.setAttribute("ime", obstojecUporabnik.getIme());
            session.setAttribute("priimek", obstojecUporabnik.getPriimek());
            session.setAttribute("email", obstojecUporabnik.getEmail());

            return "index";
        } else {
            return "prijava";
        }
    }

    @RequestMapping(value = "/odjava", method = RequestMethod.GET)
    public String logout(Model model) {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = request.getSession(true);

        session.invalidate();

        return "index";
    }

    public UporabnikRepozitorij getUporabnikRepozitorij() {
        return uporabnikRepozitorij;
    }

    public void setUporabnikRepozitorij(UporabnikRepozitorij uporabnikRepozitorij) {
        this.uporabnikRepozitorij = uporabnikRepozitorij;
    }

    public ConfirmationTokenRepository getConfirmationTokenRepository() {
        return confirmationTokenRepository;
    }

    public void setConfirmationTokenRepository(ConfirmationTokenRepository confirmationTokenRepository) {
        this.confirmationTokenRepository = confirmationTokenRepository;
    }

    public EmailSenderService getEmailSenderService() {
        return emailSenderService;
    }

    public void setEmailSenderService(EmailSenderService emailSenderService) {
        this.emailSenderService = emailSenderService;
    }
}
