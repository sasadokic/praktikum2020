package demo.controller;

import demo.model.Gradivo;
import demo.model.TipGradiva;
import demo.service.repository.GradivoRepozitorij;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Blob;
import java.util.Date;

@Controller
public class GradivoController {
    @Autowired
    GradivoRepozitorij gradivoRepozitorij;

    @RequestMapping(value = {"/dodajGradiv"}, method = RequestMethod.GET)
    public String dodaj(Model model,
                        @RequestParam(value = "naziv") String naziv, @RequestParam(value = "sifrant") String sifrant, @RequestParam(value = "opis") String opis, @RequestParam(value = "uporabnik") String uporabnik/* , @RequestParam(value = "photo") String photo */
    ) {
        gradivoRepozitorij.save(new Gradivo(naziv, sifrant, opis, uporabnik/* , photo*/));
        model.addAttribute("gradiva", gradivoRepozitorij.findAll());

        return "dodajGradivo";
    }


}
