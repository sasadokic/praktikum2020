package demo.controller;

import demo.model.Gradivo;
import demo.model.Izposoja;
import demo.model.Uporabnik;
import demo.service.repository.IzposojaRepozitorij;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;

@Controller
public class IzposojaController {
    @Autowired
    IzposojaRepozitorij izposojaRepozitorij;

    @RequestMapping(value = {"/izposodi"}, method = RequestMethod.GET)
    public String dodajI(Model model,
                         @RequestParam(value = "datumvrnitve") Date datumvrnitve, @RequestParam(value = "datum_izposoje") Date datum_izposoje, @RequestParam(value = "izposojevalec") String izposojevalec, @RequestParam(value = "gradivo") String gradivo, @RequestParam(value = "uporabnik_izposoja") String uporabnik_izposoja
    ) {
        izposojaRepozitorij.save(new Izposoja(datumvrnitve, datum_izposoje, izposojevalec,gradivo, uporabnik_izposoja));
        model.addAttribute("izposoja", izposojaRepozitorij.findAll());

        return "izposoja";
    }

    @RequestMapping (value ={"vrni"}, method = RequestMethod.GET)
    public String vrniGradivo(Model model, @RequestParam(value = "vrni") Integer id) {

        Izposoja izposoja = izposojaRepozitorij.findOne(id);
        izposoja.setZakljucena(true);
        izposojaRepozitorij.save(izposoja);

        return "prikazIzposojenegaGradiva";
    }

    @RequestMapping (value ={"vrnizamuda"}, method = RequestMethod.GET)
    public String vrniGradivoZamuda(Model model, @RequestParam(value = "vrnizamuda") Integer id) {

        Izposoja izposoja = izposojaRepozitorij.findOne(id);
        izposoja.setZakljucena(true);
        izposojaRepozitorij.save(izposoja);

        return "prikazIzposojenegaGradiva";
    }

    @RequestMapping(value = {"/qrIzposoja/{id_izposoja}"}, method = RequestMethod.GET)
    public String qrcodeIzpis(@PathVariable("id_izposoja") Integer id, Model model) {




        ArrayList<Izposoja> izposoja = new ArrayList<>();
        for (Izposoja i:izposojaRepozitorij.findAll()) {
            if (i.getId_izposoja() == id){
                izposoja.add(i);
            }
        }



        model.addAttribute("izposoja", izposoja);
        return "qrIzposoja";

    }


}
