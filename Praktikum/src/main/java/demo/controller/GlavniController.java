package demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class GlavniController {
    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Model model) {
        return "index";
    }

    @RequestMapping(value = {"/prijava"}, method = RequestMethod.GET)
    public String prijava(Model model) {
        return "prijava";
    }

    @RequestMapping(value = {"/pozabljenoGeslo"}, method = RequestMethod.GET)
    public String pozabljenoGeslo(Model model) {
        return "pozabljenoGeslo";
    }

    @RequestMapping(value = {"/spremeniGeslo"}, method = RequestMethod.GET)
    public String spremeniGeslo(Model model) {
        return "spremeniGeslo";
    }

    @RequestMapping(value = {"/gradiva"}, method = RequestMethod.GET)
    public String gradiva(Model model) {
        return "gradiva";
    }

    @RequestMapping(value = {"/dodajGradivo"}, method = RequestMethod.GET)
    public String dodajGradivo(Model model) {
        return "dodajGradivo";
    }

    @RequestMapping(value = {"/izposoja"}, method = RequestMethod.GET)
    public String izposoja(Model model) {
        return "izposoja";
    }

    @RequestMapping(value = {"/prikazIzposojenegaGradiva"}, method = RequestMethod.GET)
    public String prikazIzposojenegaGradiva(Model model) {
        return "prikazIzposojenegaGradiva";
    }

    @RequestMapping(value = {"/mojeIzposoje"}, method = RequestMethod.GET)
    public String prikazmoje(Model model) {
        return "mojeIzposoje";
    }

    @RequestMapping(value = {"/skeniranje"}, method = RequestMethod.GET)
    public String prikazmoj(Model model) {
        return "skeniranje";
    }

    @RequestMapping(value = {"/qrIzposoja"}, method = RequestMethod.GET)
    public String qrIzposoja(Model model) {
        return "qrIzposoja";
    }

}
