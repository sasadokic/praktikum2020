package demo;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;


@SpringBootApplication
public class RunApplication {
    public static void main(String[] args) throws SchedulerException {
        SpringApplication.run(RunApplication.class, args);

        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.start();

        JobDetail job = newJob(PreverjanjeZamude.class)
                .withIdentity("preveriZamudo")
                .build();

        SimpleTrigger trigger = newTrigger().withIdentity("trigger")
                .startNow()
                .withSchedule(simpleSchedule().withIntervalInHours(24).repeatForever())
                .build();

        scheduler.scheduleJob(job, trigger);


    }
}