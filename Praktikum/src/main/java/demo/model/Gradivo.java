package demo.model;

import javax.persistence.*;
import java.sql.Blob;

@Entity
public class Gradivo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id_gradivo")
    private long id_gradivo;

    private String naziv;

    private String sifrant;

    private String opis;

    private String uporabnik;

    // private String photo;

    // private String contentType = "image/png";

    public Gradivo(){
    }

    public Gradivo(String naziv, String sifrant, String opis, String uporabnik /*, String photo */) {
        this.naziv=naziv;
        this.sifrant = sifrant;
        this.opis = opis;
        this.uporabnik = uporabnik;
        //this.photo = photo;
    }

    public long getId_gradivo() {
        return id_gradivo;
    }

    public void setId_gradivo(long id_gradivo) {
        this.id_gradivo = id_gradivo;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getSifrant() {
        return sifrant;
    }

    public void setSifrant(String sifrant) {
        this.sifrant = sifrant;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    /* public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    } */


}
