package demo.model;

import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.persistence.*;

@Entity
public class Uporabnik {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_uporabnik")
    private long id_uporabnik;

    private String email;

    private String geslo;

    @Column(name = "ime")
    private String ime;

    @Column(name = "priimek")
    private String priimek;

    @Column(name = "salt")
    private String salt;


    private boolean isEnabled;

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public long getId_uporabnik() {
        return id_uporabnik;
    }

    public void setId_uporabnik(long id_uporabnik) {
        this.id_uporabnik = id_uporabnik;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGeslo() {
        return geslo;
    }

    public void setGeslo(String geslo) {
        this.geslo = geslo;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPriimek() {
        return priimek;
    }

    public void setPriimek(String priimek) {
        this.priimek = priimek;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
