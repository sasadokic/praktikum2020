package demo.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class TipGradiva {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_tip_gradiva")
    private long id_tip_gradiva;

    @Column(name = "naziv_tipa_gradiva")
    private String naziv_tipa_gradiva;


    public TipGradiva() {
    }

    public TipGradiva(String naziv_tipa_gradiva) {
        this.naziv_tipa_gradiva=naziv_tipa_gradiva;
    }

    public long getId_tip_gradiva() {
        return id_tip_gradiva;
    }

    public void setId_tip_gradiva(long id_tip_gradiva) {
        this.id_tip_gradiva = id_tip_gradiva;
    }

    public String getNaziv_tipa_gradiva() {
        return naziv_tipa_gradiva;
    }

    public void setNaziv_tipa_gradiva(String naziv_tipa_gradiva) {
        this.naziv_tipa_gradiva = naziv_tipa_gradiva;
    }
}
