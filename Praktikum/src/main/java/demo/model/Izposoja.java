package demo.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Izposoja {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id_izposoja;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.DATE)
    private Date datumvrnitve;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.DATE)
    private Date datum_izposoje;

    private String izposojevalec;

    private String gradivo;

    private boolean zakljucena;

    private String uporabnik_izposoja;


    public Izposoja(){}

    public Izposoja(Date datumvrnitve, Date datum_izposoje, String izposojevalec, String gradivo, String uporabnik_izposoja) {
        this.datum_izposoje = datum_izposoje;
        this.datumvrnitve = datumvrnitve;
        this.izposojevalec = izposojevalec;
        this.gradivo = gradivo;
        zakljucena=false;
        this.uporabnik_izposoja = uporabnik_izposoja;
    }

    public Integer getId_izposoja() {
        return id_izposoja;
    }

    public void setId_izposoja(Integer id_izposoja) {
        this.id_izposoja = id_izposoja;
    }

    public Date getDatumvrnitve() {
        return datumvrnitve;
    }

    public void setDatumvrnitve(Date datumvrnitve) {
        this.datumvrnitve = datumvrnitve;
    }

    public Date getDatum_izposoje() {
        return datum_izposoje;
    }

    public void setDatum_izposoje(Date datum_izposoje) {
        this.datum_izposoje = datum_izposoje;
    }

    public String getIzposojevalec() {
        return izposojevalec;
    }

    public void setIzposojevalec(String izposojevalec) {
        this.izposojevalec = izposojevalec;
    }

    public String getGradivo() {
        return gradivo;
    }

    public void setGradivo(String gradivo) {
        this.gradivo = gradivo;
    }

    public boolean isZakljucena() {
        return zakljucena;
    }

    public void setZakljucena(boolean zakljucena) {
        this.zakljucena = zakljucena;
    }
}
